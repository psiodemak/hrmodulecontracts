﻿using HrModule.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Domain.Repositories
{
    /// <summary>
    /// Interface for contracts repository.
    /// </summary>
    public interface IContractRepository
    {
        IEnumerable<Contract> Contracts { get; }

        void SaveContract(Contract contract);
    }
}

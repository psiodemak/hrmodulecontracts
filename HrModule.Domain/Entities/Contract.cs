﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Domain.Entities
{
    /// <summary>
    /// Simple class to descript Contract entity.
    /// </summary>
    public class Contract
    {
        [Key]
        public int ContractID { get; set; }

        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Working position is required.")]
        public PositionType WorkingAs { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Experience is required.")]
        public int Experience { get; set; }

        [Required]
        [Column(TypeName = "Money")]
        [Range(typeof(decimal), "1",  "79228162514264337593543950335", ErrorMessage = "Salary is required. Press Calculate button")]
        public decimal Salary { get; set; }
    }
}

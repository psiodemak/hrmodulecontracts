﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Domain.Entities
{
    /// <summary>
    /// Contract could be for programmer or tester.
    /// </summary>
    public enum PositionType
    {
        Programmer, Tester
    }
}

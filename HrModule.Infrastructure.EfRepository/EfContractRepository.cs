﻿using HrModule.Domain.Entities;
using HrModule.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Infrastructure.EfRepository
{
    /// <summary>
    /// Entity Framework implementation of repository
    /// </summary>
    public class EfContractRepository : IContractRepository
    {
        private EfDbContext _context = new EfDbContext();

        public IEnumerable<Contract> Contracts
        {
            get { return _context.Contracts.AsNoTracking(); }
        }

        public void SaveContract(Contract contract)
        {
            if (contract.ContractID == 0)
            {
                _context.Contracts.Add(contract);
            }
            else
            {
                Contract dbEntry = _context.Contracts.Find(contract.ContractID);
                if (dbEntry != null)
                {
                    dbEntry.Name = contract.Name;
                    dbEntry.WorkingAs = contract.WorkingAs;
                    dbEntry.Experience = contract.Experience;
                    dbEntry.Salary = contract.Salary;
                }
            }

            _context.SaveChanges();
        }
    }
}

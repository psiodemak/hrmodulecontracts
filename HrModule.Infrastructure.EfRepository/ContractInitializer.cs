﻿using HrModule.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Infrastructure.EfRepository
{
    public class ContractInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<EfDbContext>
    {
        protected override void Seed(EfDbContext context)
        {
            var contracts = new List<Contract>
                {
                    new Contract { Name="Jan Kowalski", WorkingAs=PositionType.Programmer, Experience=5, Salary=5000 },
                    new Contract { Name="Tomasz Nowak", WorkingAs=PositionType.Tester, Experience=1, Salary=1000 },
                    new Contract { Name="John Doe", WorkingAs=PositionType.Programmer, Experience=15, Salary=15000 },
                    new Contract { Name="John Smith", WorkingAs=PositionType.Programmer, Experience=7, Salary=7000 },
                    new Contract { Name="Ryszard Wolny", WorkingAs=PositionType.Programmer, Experience=2, Salary=2000 },
                    new Contract { Name="Janusz Maruda", WorkingAs=PositionType.Tester, Experience=4, Salary=4000 },
                    new Contract { Name="Natalia Mądra", WorkingAs=PositionType.Programmer, Experience=6, Salary=6000 },
                    new Contract { Name="Joanna Nowak", WorkingAs=PositionType.Tester, Experience=15, Salary=15000 },
                    new Contract { Name="Warren Buffet", WorkingAs=PositionType.Programmer, Experience=30, Salary=30000 },
                    new Contract { Name="Bill Gates", WorkingAs=PositionType.Programmer, Experience=25, Salary=25000 },
                    new Contract { Name="Mateusz Student", WorkingAs=PositionType.Programmer, Experience=2, Salary=2000 },
                };

            contracts.ForEach(s => context.Contracts.Add(s));
            context.SaveChanges();
        }
    }
}

﻿using HrModule.Application.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application
{
    /// <summary>
    /// Salary for tester.
    /// </summary>
    public class CalculateSalaryForTester : ICalculateSalaryForTester
    {
        public decimal Calculate(int experience)
        {
            int minimalSalary = 2000;

            if (experience >= 2 && experience <= 4)
            {
                minimalSalary = 2700;
            }

            if (experience > 4)
            {
                minimalSalary = 3200;
            }

            return minimalSalary + (experience * 100 + minimalSalary / 4);
        }
    }
}

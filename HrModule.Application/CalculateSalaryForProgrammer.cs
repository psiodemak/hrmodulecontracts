﻿using HrModule.Application.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application
{
    /// <summary>
    /// Salary for programmer.
    /// </summary>
    public class CalculateSalaryForProgrammer : ICalculateSalaryForProgrammer
    {
        public decimal Calculate(int experience)
        {
            int minimalSalary = 2500;

            if (experience >= 3 && experience <= 5)
            {
                minimalSalary = 5000;
            }

            if (experience > 5)
            {
                minimalSalary = 5500;
            }

            return minimalSalary + (experience * 125);
        }
    }
}

﻿using HrModule.Application.Shared;
using HrModule.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application
{
    /// <summary>
    /// Calculate salary. Uses algorithm for programmers or testers.
    /// </summary>
    public class CalculateSalaryQuery : ICalculateSalaryQuery
    {
        private readonly ICalculateSalaryForProgrammer _programmerSalary;
        private readonly ICalculateSalaryForTester _testerSalary;

        public CalculateSalaryQuery(ICalculateSalaryForProgrammer programmerSalary, ICalculateSalaryForTester testerSalary)
        {
            _programmerSalary = programmerSalary;
            _testerSalary = testerSalary;
        }

        public decimal Execute(string workingAs, int experience)
        {
            PositionType position;
            if(!Enum.TryParse<PositionType>(workingAs, out position))
            {
                throw new ArgumentOutOfRangeException("Couldn't set position type");
            }

            decimal retVal = -1;
            switch(position)
            {
                case PositionType.Programmer:
                    retVal = _programmerSalary.Calculate(experience);
                    break;
                case PositionType.Tester:
                    retVal = _testerSalary.Calculate(experience);
                    break;
            }

            return retVal;
        }
    }
}

﻿using HrModule.Application.Shared;
using HrModule.Domain.Entities;
using HrModule.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application
{
    /// <summary>
    /// Edit existing contract.
    /// </summary>
    public class EditContractCommand : IEditContractCommand
    {
        private readonly IContractRepository _repository;

        public EditContractCommand(IContractRepository repository)
        {
            _repository = repository;
        }

        public void Execute(Contract contract)
        {
            _repository.SaveContract(contract);
        }
    }
}

﻿using HrModule.Application.Shared;
using HrModule.Domain.Entities;
using HrModule.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;


namespace HrModule.Application
{
    /// <summary>
    /// Reads contracts for selected filter and page.
    /// </summary>
    public class GetContractsQuery : IGetContractsQuery
    {
        private readonly IContractRepository _repository;

        public GetContractsQuery(IContractRepository repository)
        {
            _repository = repository;
            PageSize = 10;
        }

        public int PageSize { get; set; }

        public GetContractsResponse Execute(ContractFilter currentFilter, int page = 1)
        {
            var contracts = from c in _repository.Contracts select c;

            contracts = contracts.OrderBy(c => c.ContractID);

            contracts = ProcessFilter(contracts, currentFilter);

            var pagingInfo = new PagingInfo { CurrentPage = page, ItemsPerPage = PageSize, TotalItems = contracts.Count() };

            contracts = contracts
                .Skip((page - 1) * PageSize)
                .Take(PageSize);

            return new GetContractsResponse { Contracts = contracts.ToList(), PagingInfo = pagingInfo };
        }

        public GetContractsResponse ExecuteAll()
        {
            var contracts = from c in _repository.Contracts select c;

            contracts = contracts.OrderBy(c => c.ContractID);

            return new GetContractsResponse { Contracts = contracts.ToList() };
        }

        private IEnumerable<Contract> ProcessFilter(IEnumerable<Contract> contracts, ContractFilter currentFilter)
        {
            if (!string.IsNullOrWhiteSpace(currentFilter.Name))
            {
                contracts = contracts.Where(c => c.Name.IndexOf(currentFilter.Name, StringComparison.OrdinalIgnoreCase) >= 0);
            }

            if (currentFilter.WorkingAs != null)
            {
                contracts = contracts.Where(c => c.WorkingAs == currentFilter.WorkingAs);
            }

            if (currentFilter.Experience != null)
            {
                string experienceOperator = CheckOperator(currentFilter.ExperienceOperator);
                contracts = contracts.Where("Experience " + experienceOperator + " @0", currentFilter.Experience);
            }

            if (currentFilter.Salary != null)
            {
                string salaryOperator = CheckOperator(currentFilter.SalaryOperator);
                contracts = contracts.Where("Salary " + salaryOperator + " @0", currentFilter.Salary);
            }

            return contracts;
        }

        private string CheckOperator(string operatorToCheck)
        {
            List<string> possibleOperators = new List<string> { ">", ">=", "=", "<=", "<" };
            string retVal = ">=";

            if(!string.IsNullOrWhiteSpace(operatorToCheck))
            {
                if(possibleOperators.Contains(operatorToCheck))
                {
                    retVal = operatorToCheck;
                }
            }

            return retVal;
        }
    }
}

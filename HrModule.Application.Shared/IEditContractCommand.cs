﻿using HrModule.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application.Shared
{
    public interface IEditContractCommand
    {
        void Execute(Contract contract);
    }
}

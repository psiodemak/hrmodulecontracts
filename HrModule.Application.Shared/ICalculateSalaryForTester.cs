﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application.Shared
{
    public interface ICalculateSalaryForTester
    {
        decimal Calculate(int experience);
    }
}

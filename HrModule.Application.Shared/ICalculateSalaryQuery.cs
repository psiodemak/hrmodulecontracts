﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application.Shared
{
    public interface ICalculateSalaryQuery
    {
        decimal Execute(string workingAs, int experience);
    }
}

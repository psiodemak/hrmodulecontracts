﻿using HrModule.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application.Shared
{
    /// <summary>
    /// Response used in ASP.NET MVC application. Provided by GetContractsQuery.
    /// </summary>
    public class GetContractsResponse
    {
        public List<Contract> Contracts { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}

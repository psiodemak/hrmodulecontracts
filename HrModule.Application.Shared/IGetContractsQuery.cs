﻿using HrModule.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application.Shared
{
    public interface IGetContractsQuery
    {
        GetContractsResponse Execute(ContractFilter currentFilter, int page = 1);
        GetContractsResponse ExecuteAll();
        int PageSize { get; set; }
    }
}

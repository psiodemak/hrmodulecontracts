﻿using HrModule.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application.Shared
{
    /// <summary>
    /// Collecting filter data. Filled in ASP.NET MVC application, processed in GetContractsQuery.
    /// </summary>
    public class ContractFilter
    {
        public ContractFilter()
        {
            ExperienceOperator = ">=";
            SalaryOperator = ">=";
        }

        public string Name { get; set; }
        public PositionType? WorkingAs { get; set; }
        public string ExperienceOperator { get; set; } // =, >, <, >=, <=
        public int? Experience { get; set; }
        public string SalaryOperator { get; set; } // =, >, <, >=, <=
        public int? Salary { get; set; }
    }
}

Create WPF application
1. Show list of contracts, without injection - ok
2. use DI container autofac - ok
3. Paging - ok
4. Fixed filter - ok
5. Processing list asynchronously - ok
6. Edit contract - ok
7. New contract - ok
8. Get salary on demand, async way - ok
9. show some wpf busy indicator - ok
10. themes for wpf?
11. there is a problem with refreshing data after edit in dialog - ok
12. simple validation on dialog
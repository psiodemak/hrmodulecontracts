﻿using HrModule.Application.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HrModule.PresentationMvc.Controllers
{
    public class SalaryController : Controller
    {
        private readonly ICalculateSalaryQuery _salaryQuery;

        public SalaryController(ICalculateSalaryQuery salaryQuery)
        {
            _salaryQuery = salaryQuery;
        }

        /// <summary>
        /// This is used when new contract is created. Asynchronous call calculates salary for selected data.
        /// </summary>
        /// <param name="workingas">Programmer or Tester</param>
        /// <param name="experience">In years</param>
        /// <returns>Salary value</returns>
        [HttpPost]
        public ActionResult CalculateSalary(string workingas, int experience)
        {
            decimal calculatedSalary = 0;

            try
            {
                calculatedSalary = _salaryQuery.Execute(workingas, experience);
            }
            catch(Exception ex)
            {
                // log this exception 
            }

            return Json(new { success = true, salary = calculatedSalary });
        }
    }
}
﻿using HrModule.Application.Shared;
using HrModule.Domain.Entities;
using HrModule.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HrModule.PresentationMvc.Controllers
{
    public class ContractController : Controller
    {
        private readonly IGetContractsQuery _getContractsQuery;
        private readonly IEditContractCommand _editContractCommand;

        public ContractController(IGetContractsQuery getContractsQuery, IEditContractCommand editContractCommand)
        {
            _getContractsQuery = getContractsQuery;
            _editContractCommand = editContractCommand;
        }

        /// <summary>
        /// Main list. Contains filters and reference to partial view ListData.
        /// </summary>
        /// <returns></returns>
        public ViewResult List()
        {
            return View();
        }

        /// <summary>
        /// Table with all contracts. Processing filtering and paging.
        /// </summary>
        /// <param name="currentFilter">Current filter values</param>
        /// <param name="page">Page number. Default is 1</param>
        /// <returns></returns>
        public PartialViewResult ListData(ContractFilter currentFilter, int page = 1)
        {
            ViewBag.CurrentFilter = currentFilter;

            GetContractsResponse model = new GetContractsResponse { Contracts = new List<Contract>(), PagingInfo = new PagingInfo() };

            try
            {
                model = _getContractsQuery.Execute(currentFilter, page);
            }
            catch(Exception ex)
            {
                // log this exception 
            }

            return PartialView(model);
        }

        /// <summary>
        /// Edit dialog. Used to edit existing contract or create a new one.
        /// </summary>
        /// <param name="contractId">Contract id. Id = 0 means that new contract will be created</param>
        /// <returns></returns>
        public ViewResult Edit(int contractId)
        {
            Contract contract = _getContractsQuery.ExecuteAll().Contracts
                .FirstOrDefault(c => c.ContractID == contractId);

            return View(contract);
        }

        /// <summary>
        /// Saving data about contract. Used in edit and create.
        /// </summary>
        /// <param name="contract">Contract data</param>
        /// <returns>Redirect to List or stay on current view</returns>
        [HttpPost]
        public ActionResult Edit(Contract contract)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _editContractCommand.Execute(contract);
                    TempData["message"] = string.Format("Saved {0} ", contract.Name);
                }
                catch(Exception ex)
                {
                    // log this exception 
                    TempData["message"] = string.Format("Could not save {0}. {1}", contract.Name, ex.Message);
                }

                return RedirectToAction("List");
            }
            else
            {
                return View(contract);
            }
        }

        /// <summary>
        /// Create a new contract. View edit is used.
        /// </summary>
        /// <returns></returns>
        public ViewResult Create()
        {
            return View("Edit", new Contract());
        }
    }
}
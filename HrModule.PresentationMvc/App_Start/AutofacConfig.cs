﻿using Autofac;
using Autofac.Integration.Mvc;
using HrModule.Application;
using HrModule.Application.Shared;
using HrModule.Domain.Repositories;
using HrModule.Infrastructure.EfRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HrModule.PresentationMvc.App_Start
{
    public static class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterAssemblyModules(typeof(MvcApplication).Assembly);

            builder.RegisterType<EfContractRepository>().As<IContractRepository>();
            builder.RegisterType<GetContractsQuery>().As<IGetContractsQuery>();
            builder.RegisterType<EditContractCommand>().As<IEditContractCommand>();

            builder.RegisterType<CalculateSalaryQuery>().As<ICalculateSalaryQuery>();
            builder.RegisterType<CalculateSalaryForProgrammer>().As<ICalculateSalaryForProgrammer>();
            builder.RegisterType<CalculateSalaryForTester>().As<ICalculateSalaryForTester>();

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
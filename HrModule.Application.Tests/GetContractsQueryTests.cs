﻿using FakeItEasy;
using HrModule.Application.Shared;
using HrModule.Domain.Entities;
using HrModule.Domain.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application.Tests
{
    [TestFixture]
    public class GetContractsQueryTests
    {
        [Test]
        public void GetContractsQuery_Test()
        {
            IContractRepository mockedRepository = A.Fake<IContractRepository>();

            IGetContractsQuery query = new GetContractsQuery(mockedRepository);
            var result = query.Execute(new ContractFilter());

            Assert.IsNotNull(result);

            A.CallTo(() => mockedRepository.Contracts).MustHaveHappened();
        }

        [Test]
        public void GetContractsQuery_FilterName_Test()
        {
            IContractRepository mockedRepository = PrepareRepositoryForTests();

            IGetContractsQuery query = new GetContractsQuery(mockedRepository);
            var result = query.Execute(new ContractFilter { Name = "Test1" });

            Assert.IsTrue(result.Contracts.Count == 1);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test1", StringComparison.OrdinalIgnoreCase));
        }

        [Test]
        public void GetContractsQuery_FilterWorkingAs_Test()
        {
            IContractRepository mockedRepository = PrepareRepositoryForTests();

            IGetContractsQuery query = new GetContractsQuery(mockedRepository);
            var result = query.Execute(new ContractFilter { WorkingAs = PositionType.Programmer });

            Assert.IsTrue(result.Contracts.Count == 3);
        }


        [Test]
        public void GetContractsQuery_FilterExperience_Test()
        {
            IContractRepository mockedRepository = PrepareRepositoryForTests();

            IGetContractsQuery query = new GetContractsQuery(mockedRepository);
            var result = query.Execute(new ContractFilter { Experience = 4 });

            Assert.IsTrue(result.Contracts.Count == 2);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test4", StringComparison.OrdinalIgnoreCase));
            Assert.IsTrue(result.Contracts[1].Name.Equals("Test5", StringComparison.OrdinalIgnoreCase));
        }

        [Test]
        public void GetContractsQuery_FilterExperienceExact_Test()
        {
            IContractRepository mockedRepository = PrepareRepositoryForTests();

            IGetContractsQuery query = new GetContractsQuery(mockedRepository);
            var result = query.Execute(new ContractFilter { ExperienceOperator="=", Experience = 4 });

            Assert.IsTrue(result.Contracts.Count == 1);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test4", StringComparison.OrdinalIgnoreCase));
        }

        [Test]
        public void GetContractsQuery_FilterExperienceLessThen_Test()
        {
            IContractRepository mockedRepository = PrepareRepositoryForTests();

            IGetContractsQuery query = new GetContractsQuery(mockedRepository);
            var result = query.Execute(new ContractFilter { ExperienceOperator = "<", Experience = 4 });

            Assert.IsTrue(result.Contracts.Count == 3);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test1", StringComparison.OrdinalIgnoreCase));
            Assert.IsTrue(result.Contracts[1].Name.Equals("Test2", StringComparison.OrdinalIgnoreCase));
            Assert.IsTrue(result.Contracts[2].Name.Equals("Test3", StringComparison.OrdinalIgnoreCase));
        }

        [Test]
        public void GetContractsQuery_FilterExperienceWithOperatorNotValid_Test()
        {
            IContractRepository mockedRepository = PrepareRepositoryForTests();

            IGetContractsQuery query = new GetContractsQuery(mockedRepository);
            var result = query.Execute(new ContractFilter { Experience = 4, ExperienceOperator="NotValid" });

            Assert.IsTrue(result.Contracts.Count == 2);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test4", StringComparison.OrdinalIgnoreCase));
            Assert.IsTrue(result.Contracts[1].Name.Equals("Test5", StringComparison.OrdinalIgnoreCase));
        }

        [Test]
        public void GetContractsQuery_FilterSalary_Test()
        {
            IContractRepository mockedRepository = PrepareRepositoryForTests();

            IGetContractsQuery query = new GetContractsQuery(mockedRepository);
            var result = query.Execute(new ContractFilter { Salary = 3000 });

            Assert.IsTrue(result.Contracts.Count == 4);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test2", StringComparison.OrdinalIgnoreCase));
        }

        [Test]
        public void GetContractsQuery_FilterSalaryExact_Test()
        {
            IContractRepository mockedRepository = PrepareRepositoryForTests();

            IGetContractsQuery query = new GetContractsQuery(mockedRepository);
            var result = query.Execute(new ContractFilter { SalaryOperator="=", Salary = 3000 });

            Assert.IsTrue(result.Contracts.Count == 1);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test3", StringComparison.OrdinalIgnoreCase));
        }

        [Test]
        public void GetContractsQuery_FilterSalaryLessThen_Test()
        {
            IContractRepository mockedRepository = PrepareRepositoryForTests();

            IGetContractsQuery query = new GetContractsQuery(mockedRepository);
            var result = query.Execute(new ContractFilter { SalaryOperator = "<", Salary = 3000 });

            Assert.IsTrue(result.Contracts.Count == 1);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test1", StringComparison.OrdinalIgnoreCase));
        }

        [Test]
        public void GetContractsQuery_FilterSalaryWithOperatorNotValid_Test()
        {
            IContractRepository mockedRepository = PrepareRepositoryForTests();

            IGetContractsQuery query = new GetContractsQuery(mockedRepository);
            var result = query.Execute(new ContractFilter { Salary = 3000, SalaryOperator="NotValid" });

            Assert.IsTrue(result.Contracts.Count == 4);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test2", StringComparison.OrdinalIgnoreCase));
        }

        [Test]
        public void GetContractsQuery_FilterNameAndPosition_Test()
        {
            IContractRepository mockedRepository = PrepareRepositoryForTests();

            IGetContractsQuery query = new GetContractsQuery(mockedRepository);
            var result = query.Execute(new ContractFilter { Name = "Test", WorkingAs = PositionType.Tester });

            Assert.IsTrue(result.Contracts.Count == 2);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test3", StringComparison.OrdinalIgnoreCase));
        }

        [Test]
        public void GetContractsQuery_Paging_Test()
        {
            IContractRepository mockedRepository = PrepareRepositoryForTests();

            IGetContractsQuery query = new GetContractsQuery(mockedRepository);
            query.PageSize = 2;
            var result = query.Execute(new ContractFilter(), 1);

            Assert.IsTrue(result.Contracts.Count == 2);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test1", StringComparison.OrdinalIgnoreCase));
            Assert.IsTrue(result.Contracts[1].Name.Equals("Test2", StringComparison.OrdinalIgnoreCase));

            Assert.IsTrue(result.PagingInfo.CurrentPage == 1);
            Assert.IsTrue(result.PagingInfo.TotalPages == 3);
            Assert.IsTrue(result.PagingInfo.TotalItems == 5);

            result = query.Execute(new ContractFilter(), 2);

            Assert.IsTrue(result.Contracts.Count == 2);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test3", StringComparison.OrdinalIgnoreCase));
            Assert.IsTrue(result.Contracts[1].Name.Equals("Test4", StringComparison.OrdinalIgnoreCase));

            Assert.IsTrue(result.PagingInfo.CurrentPage == 2);
            Assert.IsTrue(result.PagingInfo.TotalPages == 3);
            Assert.IsTrue(result.PagingInfo.TotalItems == 5);

            result = query.Execute(new ContractFilter(), 3);

            Assert.IsTrue(result.Contracts.Count == 1);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test5", StringComparison.OrdinalIgnoreCase));

            Assert.IsTrue(result.PagingInfo.CurrentPage == 3);
            Assert.IsTrue(result.PagingInfo.TotalPages == 3);
            Assert.IsTrue(result.PagingInfo.TotalItems == 5);
        }

        [Test]
        public void GetContractsQuery_FilterAndPaging_Test()
        {
            IContractRepository mockedRepository = PrepareRepositoryForTests();

            IGetContractsQuery query = new GetContractsQuery(mockedRepository);
            query.PageSize = 2;
            var result = query.Execute(new ContractFilter { WorkingAs = PositionType.Programmer }, 1);

            Assert.IsTrue(result.Contracts.Count == 2);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test1", StringComparison.OrdinalIgnoreCase));
            Assert.IsTrue(result.Contracts[1].Name.Equals("Test2", StringComparison.OrdinalIgnoreCase));

            Assert.IsTrue(result.PagingInfo.CurrentPage == 1);
            Assert.IsTrue(result.PagingInfo.TotalPages == 2);
            Assert.IsTrue(result.PagingInfo.TotalItems == 3);

            result = query.Execute(new ContractFilter { WorkingAs = PositionType.Programmer }, 2);

            Assert.IsTrue(result.Contracts.Count == 1);
            Assert.IsTrue(result.Contracts[0].Name.Equals("Test4", StringComparison.OrdinalIgnoreCase));

            Assert.IsTrue(result.PagingInfo.CurrentPage == 2);
            Assert.IsTrue(result.PagingInfo.TotalPages == 2);
            Assert.IsTrue(result.PagingInfo.TotalItems == 3);
        }



        private IContractRepository PrepareRepositoryForTests()
        {
            List<Contract> mockedContractsList = new List<Contract>
            {
                new Contract {ContractID = 1, Name="Test1", WorkingAs=PositionType.Programmer, Experience=1, Salary=1000 },
                new Contract {ContractID = 2, Name="Test2", WorkingAs=PositionType.Programmer, Experience=2, Salary=21000 },
                new Contract {ContractID = 3, Name="Test3", WorkingAs=PositionType.Tester, Experience=3, Salary=3000 },
                new Contract {ContractID = 4, Name="Test4", WorkingAs=PositionType.Programmer, Experience=4, Salary=4000 },
                new Contract {ContractID = 5, Name="Test5", WorkingAs=PositionType.Tester, Experience=5, Salary=5000 }
            };

            IContractRepository mockedRepository = A.Fake<IContractRepository>();
            A.CallTo(() => mockedRepository.Contracts).Returns(mockedContractsList);

            return mockedRepository;
        }
    }
}

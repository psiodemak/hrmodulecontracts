﻿using HrModule.Application.Shared;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application.Tests
{
    [TestFixture]
    public class CalculateSalaryForTesterTests
    {
        [Test]
        public void CalculateSalaryForTester_Test()
        {
            ICalculateSalaryForTester calc = new CalculateSalaryForTester();

            var juniorTesterSalary = calc.Calculate(1);
            Assert.IsTrue(juniorTesterSalary == 2600);

            var normalTesterSalary = calc.Calculate(3);
            Assert.IsTrue(normalTesterSalary == 3675);

            var seniorTesterSalary = calc.Calculate(5);
            Assert.IsTrue(seniorTesterSalary == 4500);
        }
    }
}

﻿using FakeItEasy;
using HrModule.Application.Shared;
using HrModule.Domain.Entities;
using HrModule.Domain.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application.Tests
{
    [TestFixture]
    public class EditContractCommandTests
    {
        [Test]
        public void EditContractCommand_Test()
        {
            IContractRepository mockedRepository = A.Fake<IContractRepository>();

            IEditContractCommand command = new EditContractCommand(mockedRepository);
            command.Execute(null);

            A.CallTo(() => mockedRepository.SaveContract(A<Contract>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
        }
    }
}

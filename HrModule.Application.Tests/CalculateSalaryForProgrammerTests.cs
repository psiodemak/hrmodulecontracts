﻿using HrModule.Application.Shared;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application.Tests
{
    [TestFixture]
    public class CalculateSalaryForProgrammerTests
    {
        [Test]
        public void CalculateSalaryForProgrammer_Test()
        {
            ICalculateSalaryForProgrammer calc = new CalculateSalaryForProgrammer();

            var juniorProgrammer = calc.Calculate(1);
            Assert.IsTrue(juniorProgrammer == 2625);

            var normalProgrammer = calc.Calculate(4);
            Assert.IsTrue(normalProgrammer == 5500);

            var seniorProgrammer = calc.Calculate(6);
            Assert.IsTrue(seniorProgrammer == 6250);

        }
    }
}

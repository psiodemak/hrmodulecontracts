﻿using FakeItEasy;
using HrModule.Application.Shared;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrModule.Application.Tests
{
    [TestFixture]
    public class CalculateSalaryQueryTests
    {
        [Test]
        public void CalculateSalaryQuery_Programmer_Test()
        {
            ICalculateSalaryForProgrammer salaryProgrammer = A.Fake<ICalculateSalaryForProgrammer>();
            ICalculateSalaryForTester salaryTester = A.Fake<ICalculateSalaryForTester>();

            ICalculateSalaryQuery query = new CalculateSalaryQuery(salaryProgrammer, salaryTester);
            query.Execute("Programmer", -1);

            A.CallTo(() => salaryProgrammer.Calculate(A<int>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => salaryTester.Calculate(A<int>.Ignored)).MustNotHaveHappened();
        }

        [Test]
        public void CalculateSalaryQuery_Tester_Test()
        {
            ICalculateSalaryForProgrammer salaryProgrammer = A.Fake<ICalculateSalaryForProgrammer>();
            ICalculateSalaryForTester salaryTester = A.Fake<ICalculateSalaryForTester>();

            ICalculateSalaryQuery query = new CalculateSalaryQuery(salaryProgrammer, salaryTester);
            query.Execute("Tester", -1);

            A.CallTo(() => salaryProgrammer.Calculate(A<int>.Ignored)).MustNotHaveHappened();
            A.CallTo(() => salaryTester.Calculate(A<int>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
        }

        [Test]
        public void CalculateSalaryQuery_NotValid_Test()
        {
            ICalculateSalaryForProgrammer salaryProgrammer = A.Fake<ICalculateSalaryForProgrammer>();
            ICalculateSalaryForTester salaryTester = A.Fake<ICalculateSalaryForTester>();

            ICalculateSalaryQuery query = new CalculateSalaryQuery(salaryProgrammer, salaryTester);
            Assert.Throws<ArgumentOutOfRangeException>(() => query.Execute("NotValid", -1));/* query.Execute("NotValid", -1);*/

            A.CallTo(() => salaryProgrammer.Calculate(A<int>.Ignored)).MustNotHaveHappened();
            A.CallTo(() => salaryTester.Calculate(A<int>.Ignored)).MustNotHaveHappened();
        }


    }
}

1. Current connection to db (Web.config):

  <connectionStrings>
    <add name="EfDbContext" connectionString="Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=HrModuleContracts;Integrated Security=True" providerName="System.Data.SqlClient" />
  </connectionStrings>

  EF is used in "code first" mode. Some test data (class ContractInitializer) should be created automatically. 

  ================================== SOLUTION DESCRIPTION ==========================================

  Domain
  * HrModule.Domain - only three classes. Entity for contract, enum for contrac type and repository interface. This is used in all other projects.

  Application
  * HrModule.Application.Shared - interfaces for commands and queries. Three additional types for filtering, response and paging.
  * HrModule.Application - implementation of interfaces from .Shared
  * HrModule.Application.Tests - tests

  Infrastructure
  * HrModule.Infrastructure.EfRepository - implementation of contract repository. Entity Framework is used

  Presentation
  * HrModule.PresentationMvc - ASP.NET MVC application. Shows list of contracts.

  ================================== REMARKS ==========================================

Asynchronous parts of GUI are based on AJAX and JQuery. Angular is not used.
Full logging is not implemented.

  
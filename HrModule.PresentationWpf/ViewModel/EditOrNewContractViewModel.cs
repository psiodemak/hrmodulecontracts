﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using HrModule.Application.Shared;
using HrModule.Domain.Entities;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HrModule.PresentationWpf.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class EditOrNewContractViewModel : ViewModelBase
    {
        private Contract _selectedContract;
        private readonly IGetContractsQuery _getContractsQuery;
        private readonly IEditContractCommand _editContractCommand;
        private readonly ICalculateSalaryQuery _salaryQuery;

        private ICommand _saveCommand;
        private ICommand _cancelCommand;
        private ICommand _calculateSalaryCommand;

        private bool _isBusy;

        /// <summary>
        /// Initializes a new instance of the EditOrNewContractViewModel class.
        /// </summary>
        public EditOrNewContractViewModel(IGetContractsQuery getContractsQuery, IEditContractCommand editContractCommand, ICalculateSalaryQuery salaryQuery)
        {
            _getContractsQuery = getContractsQuery;
            _editContractCommand = editContractCommand;
            _salaryQuery = salaryQuery;
            _isBusy = false;
        }

        public void PrepareData(int contractId)
        {
            ContractId = contractId;

            if (ContractId != 0)
            {
                var check = (from ctr in _getContractsQuery.ExecuteAll().Contracts where ctr.ContractID == ContractId select ctr).FirstOrDefault();
                if (check != null)
                {
                    SelectedContract = check;
                }
            }
            else
            {
                SelectedContract = new Contract();
            }
        }

        public int ContractId { get; set; }

        public Contract SelectedContract
        {
            get
            {
                return _selectedContract;
            }

            set
            {
                _selectedContract = value;
                RaisePropertyChanged();
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                if (this._saveCommand == null)
                {
                    this._saveCommand = new RelayCommand(this.Save);
                }

                return this._saveCommand;
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                if (this._cancelCommand == null)
                {
                    this._cancelCommand = new RelayCommand(this.Cancel);
                }

                return this._cancelCommand;
            }
        }

        public ICommand CalculateSalaryCommand
        {
            get
            {
                if(this._calculateSalaryCommand == null)
                {
                    this._calculateSalaryCommand = new RelayCommand(this.CalculateSalary);
                }

                return this._calculateSalaryCommand;
            }
        }

        public Action CloseAction { get; set; }

        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }

            set
            {
                _isBusy = value;
                RaisePropertyChanged();
            }
        }

        private void Save()
        {
            _editContractCommand.Execute(this.SelectedContract);
            CloseAction?.Invoke();
        }

        private void Cancel()
        {
            CloseAction?.Invoke();
        }

        private void CalculateSalary()
        {
            CallerWithAsync(this.SelectedContract.WorkingAs.ToString(), this.SelectedContract.Experience);
        }

        private decimal GetSalary(string workingAs, int experience)
        {
            Thread.Sleep(2000);

            return _salaryQuery.Execute(workingAs, experience);
        }

        private Task<decimal> GetSalaryAsync(string workingAs, int experience)
        {
            return Task.Run<decimal>(() =>
            {
                return GetSalary(workingAs, experience);
            });

        }

        private async void CallerWithAsync(string workingAs, int experience)
        {
            IsBusy = true;
            decimal salary = await this.GetSalaryAsync(workingAs, experience);

            this.SelectedContract.Salary = salary;
            RaisePropertyChanged("SelectedContract");
            IsBusy = false;
        }

    }
}
﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using HrModule.Application;
using HrModule.Application.Shared;
using HrModule.Domain.Entities;
using HrModule.Domain.Repositories;
using HrModule.Infrastructure.EfRepository;
using HrModule.PresentationWpf.Model;
using HrModule.PresentationWpf.View;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HrModule.PresentationWpf.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IContractRepository _repository;
        private readonly IGetContractsQuery _getContractsQuery;

        /// <summary>
        /// The <see cref="WelcomeTitle" /> property's name.
        /// </summary>
        public const string WelcomeTitlePropertyName = "WelcomeTitle";

        private string _welcomeTitle = string.Empty;

        private List<Contract> _contractsPage;

        private Contract _selectedContract;

        private int _currentPage;
        private int _totalPages;
        private string _pageDescription;

        private ICommand _nextPageCommand;
        private ICommand _prevPageCommand;

        private ICommand _experience5YearsCommand;
        private ICommand _salaryMoreThan5kCommand;
        private ICommand _clearFilterCommand;

        private ContractFilter _currentFilter = new ContractFilter();

        private ICommand _showDetailCommand;
        private ICommand _addContractCommand;

        /// <summary>
        /// Gets the WelcomeTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string WelcomeTitle
        {
            get
            {
                return _welcomeTitle;
            }
            set
            {
                Set(ref _welcomeTitle, value);
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IContractRepository repository, IGetContractsQuery getContractsQuery)
        {
            _repository = repository;
            _getContractsQuery = getContractsQuery;

            _welcomeTitle = "Hello from MvvmLight";

            _currentPage = 1;
            CallerWithAsync(_currentPage);
        }

        public List<Contract> ContractsPage
        {
            get
            {
                return _contractsPage;
            }

            set
            {
                _contractsPage = value;
                RaisePropertyChanged();
            }
        }

        public Contract SelectedContract
        {
            get
            {
                return _selectedContract;
            }

            set
            {
                _selectedContract = value;
                RaisePropertyChanged();
            }
        }

        public string PageDescription
        {
            get
            {
                return _pageDescription;
            }

            set
            {
                _pageDescription = value;
                RaisePropertyChanged();
            }
        }

        private void SetPageDescription()
        {
            PageDescription = "Page " + _currentPage + " of " + _totalPages;
        }

        public ICommand NextPageCommand
        {
            get
            {
                if (this._nextPageCommand == null)
                {
                    this._nextPageCommand = new RelayCommand(this.NextPage);
                }

                return this._nextPageCommand;
            }
        }

        public ICommand PrevPageCommand
        {
            get
            {
                if (this._prevPageCommand == null)
                {
                    this._prevPageCommand = new RelayCommand(this.PrevPage);
                }

                return this._prevPageCommand;
            }
        }

        public ICommand Experience5YearsCommand
        {
            get
            {
                if (this._experience5YearsCommand == null)
                {
                    this._experience5YearsCommand = new RelayCommand(this.FilterExp5Years);
                }

                return this._experience5YearsCommand;
            }
        }

        public ICommand SalaryMoreThan5kCommand
        {
            get
            {
                if (this._salaryMoreThan5kCommand == null)
                {
                    this._salaryMoreThan5kCommand = new RelayCommand(this.FilterSalaryMoreThan5k);
                }

                return this._salaryMoreThan5kCommand;
            }
        }

        public ICommand ClearFilterCommand
        {
            get
            {
                if (this._clearFilterCommand == null)
                {
                    this._clearFilterCommand = new RelayCommand(this.FilterClear);
                }

                return this._clearFilterCommand;
            }
        }

        public ICommand ShowDetailCommand
        {
            get
            {
                if (this._showDetailCommand == null)
                {
                    this._showDetailCommand = new RelayCommand<int>(this.ShowDetail);
                }

                return this._showDetailCommand;
            }
        }

        public ICommand AddContractCommand
        {
            get
            {
                if (this._addContractCommand == null)
                {
                    this._addContractCommand = new RelayCommand(this.AddContract);
                }

                return this._addContractCommand;
            }

        }

        private void NextPage()
        {
            if (_currentPage + 1 <= _totalPages)
            {
                _currentPage++;
                ContractsPage = new List<Contract>();
                CallerWithAsync(_currentPage);
            }
        }

        private void PrevPage()
        {
            if (_currentPage > 1)
            {
                _currentPage--;
                ContractsPage = new List<Contract>();
                CallerWithAsync(_currentPage);
            }
        }

        private GetContractsResponse GetContractData(int currentPage)
        {
            //Thread.Sleep(2000);

            return _getContractsQuery.Execute(_currentFilter, _currentPage);
        }

        private Task<GetContractsResponse> GetContractDataAsync(int currentPage)
        {
            return Task.Run<GetContractsResponse>(() =>
            {
                return GetContractData(currentPage);
            });

        }

        private async void CallerWithAsync(int currentPage)
        {
            GetContractsResponse response = await this.GetContractDataAsync(currentPage);

            ContractsPage = response.Contracts;

            _totalPages = response.PagingInfo.TotalPages;
            SetPageDescription();
        }

        private void FilterClear()
        {
            _currentFilter = new ContractFilter();

            _currentPage = 1;
            ContractsPage = new List<Contract>();
            CallerWithAsync(_currentPage);
        }

        private void FilterExp5Years()
        {
            _currentFilter = new ContractFilter();
            _currentFilter.Experience = 5;
            _currentFilter.ExperienceOperator = "=";

            _currentPage = 1;
            ContractsPage = new List<Contract>();
            CallerWithAsync(_currentPage);
        }

        private void FilterSalaryMoreThan5k()
        {
            _currentFilter = new ContractFilter();
            _currentFilter.Salary = 5000;

            _currentPage = 1;
            ContractsPage = new List<Contract>();
            CallerWithAsync(_currentPage);
        }

        private void ShowDetail(int contractId)
        {
            EditOrNewContractView view = new EditOrNewContractView();
            ((EditOrNewContractViewModel)view.DataContext).PrepareData(contractId);
            view.ShowDialog();

            ContractsPage = new List<Contract>();
            CallerWithAsync(_currentPage);
        }

        private void AddContract()
        {
            EditOrNewContractView view = new EditOrNewContractView();
            ((EditOrNewContractViewModel)view.DataContext).PrepareData(0);
            view.ShowDialog();

            ContractsPage = new List<Contract>();
            CallerWithAsync(_currentPage);
        }
    }
}
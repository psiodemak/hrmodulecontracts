﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:HrModule.PresentationWpf.ViewModel"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using HrModule.PresentationWpf.Model;
using Autofac;
using HrModule.Infrastructure.EfRepository;
using HrModule.Application;
using HrModule.Domain.Repositories;
using HrModule.Application.Shared;

namespace HrModule.PresentationWpf.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class ViewModelLocator
    {
        private static IContainer _container;

        static ViewModelLocator()
        {
            //ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            //if (ViewModelBase.IsInDesignModeStatic)
            //{
            //    SimpleIoc.Default.Register<IDataService, Design.DesignDataService>();
            //}
            //else
            //{
            //    SimpleIoc.Default.Register<IDataService, DataService>();
            //}

            //SimpleIoc.Default.Register<MainViewModel>();

            _container = Bootstrap();
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel Main
        {
            get
            {
                //return ServiceLocator.Current.GetInstance<MainViewModel>();
                return _container.Resolve<MainViewModel>();
            }
        }

        public EditOrNewContractViewModel EditOrNew
        {
            get
            {
                return _container.Resolve<EditOrNewContractViewModel>();
            }
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {
        }

        private static IContainer Bootstrap()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<MainViewModel>().AsSelf();
            builder.RegisterType<EditOrNewContractViewModel>().AsSelf();

            builder.RegisterType<EfContractRepository>().As<IContractRepository>();
            builder.RegisterType<GetContractsQuery>().As<IGetContractsQuery>();
            builder.RegisterType<EditContractCommand>().As<IEditContractCommand>();

            builder.RegisterType<CalculateSalaryQuery>().As<ICalculateSalaryQuery>();
            builder.RegisterType<CalculateSalaryForProgrammer>().As<ICalculateSalaryForProgrammer>();
            builder.RegisterType<CalculateSalaryForTester>().As<ICalculateSalaryForTester>();

            return builder.Build();
        }
    }
}
﻿using HrModule.PresentationWpf.ViewModel;
using System.Windows;

namespace HrModule.PresentationWpf.View
{
    /// <summary>
    /// Description for EditOrNewContractView.
    /// </summary>
    public partial class EditOrNewContractView : Window
    {
        /// <summary>
        /// Initializes a new instance of the EditOrNewContractView class.
        /// </summary>
        public EditOrNewContractView()
        {
            InitializeComponent();

            if (this.DataContext != null)
            {
                var vm = this.DataContext as EditOrNewContractViewModel;
                if (vm != null)
                {
                    vm.CloseAction = () => this.Close();
                }
            }
        }
    }
}